﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TraspasoVehicular.Models;
using TraspasoVehicular.Models.ViewModels;

namespace TraspasoVehicular.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Index()
        {
            List<V_Cliente> lst;
            using (TraspasoVehicularEntities db= new TraspasoVehicularEntities())
            {
                lst = (from d in db.CLIENTE
                           select new V_Cliente
                           {
                               CORR_CLIENTE = d.CORR_CLIENTE,
                               PRIMER_NOMBRE = d.PRIMER_NOMBRE,
                               SEGUNDO_NOMBRE = d.SEGUNDO_NOMBRE,
                               PRIMER_APELLIDO = d.PRIMER_APELLIDO,
                               SEGUNDO_APELLIDO = d.SEGUNDO_APELLIDO,
                               EDAD = d.EDAD,
                               PROFESION = d.PROFESION,
                               DOMICILIO = d.DOMICILIO,
                               DUI = d.DUI,
                               NIT = d.NIT,
                               CORR_TIPO_CLIENTE = d.CORR_TIPO_CLIENTE
                           }).ToList();
            }
                return View(lst);
        }
    }
}
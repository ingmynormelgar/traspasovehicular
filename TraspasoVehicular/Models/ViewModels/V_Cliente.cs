﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TraspasoVehicular.Models.ViewModels
{
    public class V_Cliente
    {
        public int CORR_CLIENTE { get; set; }
        public string PRIMER_NOMBRE { get; set; }
        public string SEGUNDO_NOMBRE { get; set; }
        public string PRIMER_APELLIDO { get; set; }
        public string SEGUNDO_APELLIDO { get; set; }
        public Nullable<int> EDAD { get; set; }
        public string PROFESION { get; set; }
        public string DOMICILIO { get; set; }
        public string DUI { get; set; }
        public string NIT { get; set; }
        public Nullable<int> CORR_TIPO_CLIENTE { get; set; }

    }
}